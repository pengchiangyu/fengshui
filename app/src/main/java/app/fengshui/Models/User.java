package app.fengshui.Models;

public class User {
    private String Name;
    private Integer Phone;

    public User(String name, Integer phone) {
        Name = name;
        Phone = phone;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getPhone() {
        return Phone;
    }

    public void setPhone(Integer phone) {
        Phone = phone;
    }

    public User(){}
}
