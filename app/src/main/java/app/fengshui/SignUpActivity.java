package app.fengshui;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import app.fengshui.Models.User;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends BaseActivity {

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @BindView(R.id.sign_up_button)
    Button signUpButton;

    @BindView(R.id.email)
    EditText email;

    @BindView(R.id.password)
    EditText password;

    @BindView(R.id.name)
    EditText name;

    @BindView(R.id.phone)
    EditText phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        password.setTransformationMethod(new PasswordHelper());
    }

    @OnClick(R.id.sign_up_button)
    public void onSignUpButtonClicked(){
        showProgressDialog();

        if(!tryParseInt(phone.getText().toString()))
        {
            hideProgressDialog();
            Toast.makeText(SignUpActivity.this, "Input only number at phone field!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        if(!validateFields(new EditText[]{email, password, name, phone})){
            hideProgressDialog();
            Toast.makeText(SignUpActivity.this, "All the fields cannot be empty!!",
                    Toast.LENGTH_LONG).show();
            return;
        }

        String email = this.email.getText().toString();
        String password = this.password.getText().toString();

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            CreateUserDatabase();
                            hideProgressDialog();
                            Toast.makeText(SignUpActivity.this, "User created! Proceed to login!",
                                    Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                            // Sign in success, update UI with the signed-in user's information
                        } else {
                            // If sign in fails, display a message to the user.
                            hideProgressDialog();
                            Log.w("Firebase: ", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignUpActivity.this, task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    private boolean validateFields(EditText[] fields){
        for (EditText currentField : fields) {
            if (currentField.getText().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }

    boolean tryParseInt(String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void CreateUserDatabase(){
        User user = new User(name.getText().toString(), Integer.parseInt(phone.getText().toString()));

        mDatabase.child("Users").child(email.getText().toString().replace(".","")).setValue(user);
    }
}
